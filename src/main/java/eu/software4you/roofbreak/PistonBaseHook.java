package eu.software4you.roofbreak;

import eu.software4you.ulib.core.inject.*;
import eu.software4you.ulib.core.reflect.ReflectUtil;
import eu.software4you.ulib.minecraft.mappings.MixedMapping;
import org.bukkit.World;
import org.bukkit.block.Block;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;

public final class PistonBaseHook {

    private final MixedMapping mappings;
    private final BiFunction<Object, World, Block> BLOCK_CONVERT;
    private final BiFunction<World, Block, Boolean> shouldAllow;

    public PistonBaseHook(MixedMapping mappings, BiFunction<World, Block, Boolean> shouldAllow) throws Exception {
        this.mappings = mappings;
        this.shouldAllow = shouldAllow;
        this.BLOCK_CONVERT = initBlockConverter(mappings);
    }

    private BiFunction<Object, World, Block> initBlockConverter(MixedMapping mappings) {
        // resolve class
        var clazz = mappings.fromSource("net.minecraft.core.Vec3i")
                .orElseThrow();

        // resolve fields
        String getX = clazz.methodFromSource("getX")
                .orElseThrow().mappedName();
        String getY = clazz.methodFromSource("getY")
                .orElseThrow().mappedName();
        String getZ = clazz.methodFromSource("getZ")
                .orElseThrow().mappedName();


        return (nmsBlockPos, world) -> {
            // fetch fields
            int x = ReflectUtil.icall(Integer.class, nmsBlockPos, getX + "()")
                    .orElseThrow();
            int y = ReflectUtil.icall(Integer.class, nmsBlockPos, getY + "()")
                    .orElseThrow();
            int z = ReflectUtil.icall(Integer.class, nmsBlockPos, getZ + "()")
                    .orElseThrow();

            // get block
            return world.getBlockAt(x, y, z);
        };
    }

    void performInjection() throws Exception {
        var spec = InjectUtil.createHookingSpec(HookPoint.FIELD_READ,
                "Lio/papermc/paper/configuration/GlobalConfiguration$UnsupportedSettings;allowPermanentBlockBreakExploits;Z");


        // resolve target method to inject
        // original name:  net.minecraft.world.level.block.piston.PistonBaseBlock
        // mapped in 1.19: net.minecraft.world.level.block.piston.BlockPiston
        var clazz = mappings.fromSource("net.minecraft.world.level.block.piston.PistonBaseBlock")
                .orElseThrow();
        var method = clazz.methodFromSource("triggerEvent")
                .orElseThrow();
        var target = method.find().orElseThrow();


        // for parameter capturing
        final Map<Thread, Object[]> capture = new ConcurrentHashMap<>();

        new HookInjection()
                // capture parameters
                .addHook(target, InjectUtil.createHookingSpec(HookPoint.HEAD), (params, cb) ->
                        capture.put(Thread.currentThread(), params))

                // the main hook
                .<Boolean>addHook(target, spec, (params, cb) ->
                        proxy_triggerEvent_read_allowPermanentBlockBreakExploits(capture.get(Thread.currentThread()), cb))

                // clean parameters
                .addHook(target, InjectUtil.createHookingSpec(HookPoint.RETURN), (params, cb) ->
                        capture.remove(Thread.currentThread()))

                .inject()
                .rethrow();
    }

    private void proxy_triggerEvent_read_allowPermanentBlockBreakExploits(Object[] params, Callback<Boolean> cb) {

        Object nmsWorld = params[1];
        Object nmsBlockPos = params[2];


        var world = ReflectUtil.icall(World.class, nmsWorld, "getWorld()")
                .orElseThrow();
        var block = BLOCK_CONVERT.apply(nmsBlockPos, world);


        if (shouldAllow.apply(world, block))
            cb.setReturnValue(true); // overwrite field read and return `true`

    }

}
