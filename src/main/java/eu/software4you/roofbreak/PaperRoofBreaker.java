package eu.software4you.roofbreak;

import eu.software4you.ulib.loader.install.Installer;
import eu.software4you.ulib.minecraft.mappings.Mappings;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;

public final class PaperRoofBreaker extends JavaPlugin {

    private PistonBaseHook hook;

    @Override
    public void onLoad() {
        // for somewhat reason a manual installation is required -> bug in ulib
        Installer.installMe();
    }

    @Override
    public void onEnable() {

        // loading mappings can take some time -> async
        getServer().getScheduler().runTaskAsynchronously(this, () -> {
            getLogger().info("Loading mappings...");

            var mappings = Mappings.getMixedMapping();

            getLogger().info("Injecting ...");

            try {
                hook = new PistonBaseHook(mappings, PaperRoofBreaker.this::shouldAllowPistonExploit);
                hook.performInjection();
            } catch (Exception e) {
                getLogger().log(Level.WARNING, "Injection failed", e);
                return;
            }

            getLogger().info("Done.");
        });

    }

    private boolean shouldAllowPistonExploit(World w, Block b) {

        if (w.getEnvironment() != World.Environment.NETHER)
            return false; // only nether

        // allow exploit in a range of 124 <= y <= 130
        return b.getY() >= 124 && b.getY() <= 130;
    }
}
